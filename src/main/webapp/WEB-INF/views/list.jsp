<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>员工列表</title>
<%
	//引入服务器绝对路径
	pageContext.setAttribute("App_Path", request.getContextPath());
%>
<!-- 引入jQuery -->
<%-- <script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script> --%>
<%-- <script type="text/javascript" src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
 --%>
<script type="text/javascript"
	src="${App_Path }/static/js/jquery-3.2.1.js"></script>
<!-- 引入样式 -->
<link
	href="${App_Path }/static/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="${App_Path }/static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>
	<%--搭建显示页面 --%>
	<div class="container">
		<%--标题行 --%>
		<div class="row">
			<div class="col-md-12">
				<h1>SSM-CRUD员工列表</h1>
			</div>
		</div>
		<%--按钮行 --%>
		<div class="row">
			<div class="col-md-4 col-md-offset-8">
				<button type="button" class="btn btn-primary">新增</button>
				<button type="button" class="btn btn-danger">删除</button>
			</div>
		</div>
		<%--列表行 --%>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-hover">
					<tr>
						<th>empId</th>
						<th>empName</th>
						<th>gender</th>
						<th>email</th>
						<th>deptName</th>
						<th>operation</th>
					</tr>
					<c:forEach items="${pageInfo.list }" var="emp">
						<tr>
							<td>${emp.empId}</td>
							<td>${emp.empName }</td>
							<td>${emp.gender }</td>
							<td>${emp.email }</td>
							<td>${emp.department.deptName }</td>
							<td>
								<button type="button" class="btn btn-primary  btn-sm">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>新增
								</button>
								<button type="button" class="btn btn-danger  btn-sm">
									<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>删除
								</button>
							</td>
						</tr>
					</c:forEach>

				</table>
			</div>
		</div>
		<%--分页信息 页码行 --%>
		<div class="row">
			<%--文字信息 --%>
			<div class="col-md-6">当前第${pageInfo.pageNum }页,共${pageInfo.pages }页,总记录数${pageInfo.total }
			</div>

			<%--页码条--%>
			<div class="col-md-6">
				<nav aria-label="Page navigation">
				<ul class="pagination">
					<li><a
						href="${App_Path }/emps?pageNumber=1">首页</a></li>
					<c:if test="${pageInfo.hasPreviousPage }">
						<li><a
							href="${App_Path }/emps?pageNumber=${pageInfo.pageNum - 1}"
							aria-label="Previous"> 
							<span aria-hidden="true">&laquo;</span>
						</a></li>
					</c:if>
					<c:forEach items="${pageInfo.navigatepageNums }" var="pageNum">
						<c:if test="${pageNum == pageInfo.pageNum }">
							<li class="active"><a href="#">${pageNum }</a></li>
						</c:if>
						<c:if test="${pageNum != pageInfo.pageNum }">
							<li><a href="${App_Path }/emps?pageNumber=${pageNum }">${pageNum }</a></li>
						</c:if>
					</c:forEach>

					<c:if test="${pageInfo.hasNextPage }">
						<li><a
							href="${App_Path }/emps?pageNumber=${pageInfo.pageNum + 1}"
							aria-label="Next"> <span aria-hidden="true">&raquo;</span>
						</a></li>
					</c:if>
					<li><a
						href="${App_Path }/emps?pageNumber=${pageInfo.pages}">末页</a></li>
				</ul>
				</nav>
			</div>
		</div>
	</div>
</body>
</html>