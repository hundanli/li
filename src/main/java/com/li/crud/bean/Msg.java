package com.li.crud.bean;
/*
 * 定义一个封装通用数据的对象
 */

import java.util.HashMap;
import java.util.Map;

public class Msg {
	//状态码
	private int code ;
	
	//请求成功
	private static final int SUCCESS = 100;
	
	//请求失败
	private static final int FAILURE = 200;
	
	//请求成功或失败的提示信息
	private String msg;
	//封装返回数据
	private Map<String , Object> extend = new HashMap<>();
	
	//返回请求成功的实例
	public static Msg success() {
		Msg result = new Msg();
		result.setCode(SUCCESS);
		result.setMsg("请求成功！");
		return result;
	}
	
	//返回请求失败的实例
	public static Msg fail() {
		Msg result = new Msg();
		result.setCode(FAILURE);
		result.setMsg("请求失败！");
		return result;
	}
	
	//封装数据
	public Msg addData(String key, Object value) {
		extend.put(key, value);
		return this;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Map<String, Object> getExtend() {
		return extend;
	}
	public void setExtend(Map<String, Object> data) {
		this.extend = data;
	}
	
	
}
