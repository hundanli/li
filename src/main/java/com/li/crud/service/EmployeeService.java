package com.li.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.li.crud.bean.Employee;
import com.li.crud.bean.EmployeeExample;
import com.li.crud.bean.EmployeeExample.Criteria;
import com.li.crud.dao.EmployeeMapper;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeMapper employeeMapper;
	/*
	 * 查询所有员工
	 */
	public List<Employee> getAllEmps() {
		return employeeMapper.selectByExampleWithDept(null);
	}
	public void saveEmp(Employee employee) {
		employeeMapper.insertSelective(employee);
	}
	
	/*
	 * 检查用户名是否可用，即数据库中是否已经存在
	 * @return 返回true，说明该用户名可用
	 */
	public boolean checkUser(String empName) {
		EmployeeExample example = new EmployeeExample();
		Criteria criteria = example.createCriteria();
		criteria.andEmpNameEqualTo(empName);
		long count = employeeMapper.countByExample(example);
		return count == 0;
	}
	
	/*
	 * 根据id查询员工信息
	 */
	public Employee getEmpById(Integer id) {
		Employee employee = employeeMapper.selectByPrimaryKey(id);
		return employee;
	}
	
	/*
	 * 更新员工信息
	 */
	public boolean updateEmp(Employee employee) {
		employeeMapper.updateByPrimaryKeySelective(employee);
//		if (i > 0 ) {
//			return true;
//		}
		return true;
	}
	
	//单个删除
	public void deleteEmp(Integer id) {
		employeeMapper.deleteByPrimaryKey(id);
	}
	
	//批量删除
	public void deleteBatch(List<Integer> idList){
		
		EmployeeExample example = new EmployeeExample();
		Criteria criteria = example.createCriteria();
		criteria.andEmpIdIn(idList);
		employeeMapper.deleteByExample(example);
	}
}
