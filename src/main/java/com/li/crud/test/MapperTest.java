//package com.li.crud.test;
//
//import java.util.UUID;
//
//import org.apache.ibatis.session.SqlSession;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.li.crud.bean.Department;
//import com.li.crud.bean.Employee;
//import com.li.crud.dao.DepartmentMapper;
//import com.li.crud.dao.EmployeeMapper;
//
//
//
//
///*
// * 使用spring test单元测试：
// * 1、导入spring test jar 包
// * 2、@ContextConfiguration注解中locations属性指定spring配置文件的位置
// * 3.@RunWith(SpringJUnit4ClassRunner.class)注解
// * 
// * 然后便可以执行autowired自动装配
// */
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations= {"classpath:applicationContext.xml"})
//public class MapperTest {
//	
//	@Autowired
//	DepartmentMapper departmentMapper;
//	@Autowired
//	EmployeeMapper empolyeeMapper;
//	@Autowired
//	SqlSession sqlSession;
//	
//	@Test
//	public void testCustom() {
//		@SuppressWarnings("resource")
//		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");
//		DepartmentMapper mapper = applicationContext.getBean(DepartmentMapper.class);
//		Department department = mapper.selectByPrimaryKey(1);
//		System.out.println(department);
//	}
//	@Test
//	public void testCRUD() {
//		
//		
//		System.out.println(departmentMapper);
//		System.out.println(empolyeeMapper);
//		
//		//测试插入部门
////		departmentMapper.insertSelective(new Department(null,"研发部"));
////		departmentMapper.insertSelective(new Department(null,"测试部"));
//		
//		//插入员工
////		empolyeeMapper.insertSelective(new Employee(null,"li","男","li@qq.com",1));
//		
//		//批量插入员工
//		EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
//		for(int i = 0; i< 100;i++) {
//			String name = UUID.randomUUID().toString().substring(0, 5);
//			mapper.insertSelective(new Employee(null, name, "女",name + "@qq.com",2));
//		}
//		System.out.println("批量完成！");
//	}
//	
//}
