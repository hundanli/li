package com.li.crud.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.li.crud.bean.Employee;
import com.li.crud.bean.Msg;
import com.li.crud.service.EmployeeService;

@Controller
public class EmployeeController {

	@Autowired
	EmployeeService service;

	/*
	 * 查询所有员工 1.为了实现分页查询，request需传入一个页数pageNumber,默认值为1 2.引入pageHelper 插件辅助分页操作
	 */
	// @RequestMapping("/emps")
	public String getEmps(@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber, Model model) {
		// 查询之前，传入页码以及每页的记录数即可
		PageHelper.startPage(pageNumber, 5);
		// 查询操作
		List<Employee> allEmps = service.getAllEmps();
		// 查询之后，将结果集封装到PageInfo中，PageInfo其中包含会分页的详细信息和数据
		// 在构造方法中还可以设置页码导航的连续显示页码数，如显示3，4，5，6，7页按钮
		PageInfo<Employee> pageInfo = new PageInfo<>(allEmps, 5);
		model.addAttribute("pageInfo", pageInfo);
		return "list";
	}

	/*
	 * 返回json格式的数据
	 */
	@RequestMapping("/emps")
	@ResponseBody // 为了返回json
	public Msg getEmpsWithJson(@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber) {
		// 查询之前，传入页码以及每页的记录数即可
		PageHelper.startPage(pageNumber, 5);
		// 查询操作
		List<Employee> allEmps = service.getAllEmps();
		// 查询之后，将结果集封装到PageInfo中，PageInfo其中包含会分页的详细信息和数据
		// 在构造方法中还可以设置页码导航的连续显示页码数，如显示3，4，5，6，7页按钮
		PageInfo<Employee> pageInfo = new PageInfo<>(allEmps, 5);
		// 把分页数据封装到msg对象中
		return Msg.success().addData("pageInfo", pageInfo);
	}

	/*
	 * 处理POST请求 根据表单数据给参数封装属性，表单元素名应该与对象属性名保持一致
	 */
	@RequestMapping(value = "/emp", method = RequestMethod.POST)
	@ResponseBody
	public Msg savaEmp(@Valid Employee employee, BindingResult result) {
		// 后端JSR303校验结果
		if (result.getErrorCount() > 0) {
			Map<String, String> map = new HashMap<>();
			List<FieldError> fieldErrors = result.getFieldErrors();
			for (FieldError fieldError : fieldErrors) {
				String field = fieldError.getField();
				String message = fieldError.getDefaultMessage();
				map.put(field, message);
			}

			return Msg.fail().addData("filedError", map);
		}

		service.saveEmp(employee);
		return Msg.success();
	}

	@ResponseBody
	@RequestMapping("/checkUser")
	public Msg checkUser(@RequestParam("empName") String empName) {
		// 首先校验表达式是否合法
		String regex = "(^[a-zA-Z0-9_-]{6,16}$)|(^[\\u2E80-\\u9FFF]{2,5}$)";
		if (!empName.matches(regex)) {
			return Msg.fail().addData("back_validate", "用户名必须是6-16位字母或者2-5位汉字");
		}

		// 查询数据库进行重复性校验
		boolean b = service.checkUser(empName);
		if (b) {
			return Msg.success();
		} else {
			return Msg.fail().addData("back_validate", "用户名已存在！");
		}
	}

	/*
	 * 根据id查询员工
	 */
	@ResponseBody
	@RequestMapping(value = "/emp/{id}", method = RequestMethod.GET)
	public Msg getEmp(@PathVariable("id") Integer id) {
		Employee employee = service.getEmpById(id);
		return Msg.success().addData("emp", employee);
	}

	/**
	 * 更新员工数据
	 * 
	 * @param employee
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/emp/{empId}", method = RequestMethod.PUT)
	public Msg saveEmp(@Valid Employee employee, BindingResult result) {
		// 后端JSR303校验结果
		if (result.getErrorCount() > 0) {
			Map<String, String> map = new HashMap<>();
			List<FieldError> fieldErrors = result.getFieldErrors();
			for (FieldError fieldError : fieldErrors) {
				String field = fieldError.getField();
				String message = fieldError.getDefaultMessage();
				map.put(field, message);
			}

			return Msg.fail().addData("filedError", map);
		}
		
		service.updateEmp(employee);
		return Msg.success();
	}
	
	//单个删除与批量删除
	@RequestMapping(value="/emp/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public Msg delete(@PathVariable("ids")String ids) {
		
		List<Integer> idList = new ArrayList<>();
		//批量删除
		if (ids.contains("-")) {
			String[] split = ids.split("-");
			for (String id : split) {
				idList.add(Integer.parseInt(id));
			}
			service.deleteBatch(idList);
		} else {
			//单个删除
			Integer id = Integer.parseInt(ids);
			service.deleteEmp(id);
		}
		
		return Msg.success();
	}
}
